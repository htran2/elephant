import React, { PureComponent } from 'react';
import { Tag, Card } from 'base-components';

class Home extends PureComponent {
  render() {
    return (
      <div className="home">
        <Tag text="This is home page" colour="gold"/>
        <Tag text="Yeah it's the home page" colour="lime"/>
        <Card title="SOME THING" contents="About something"/>
      </div>
    );
  }
}

export default Home;
