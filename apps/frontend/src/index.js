import React from 'react';
import ReactDOM from 'react-dom';
import 'base-components/build/index.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/*
TO CONTINUE:

UI Components: add Ant Design

API Add param 'id' to /client
Front end: add router to /client?id=123
Clients : makes a <Client> component

Do something so that:

- in the frontend you can say <ClientInfo id="123" />

which means, here you make the component ClientInfo,
it reads props id, query api for data,
*/
