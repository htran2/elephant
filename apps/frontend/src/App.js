import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Client from './Client';
import Home from './Home';

function App() {
  return (
    <div className="app">
      <div style={{ fontSize: "3rem" }}>Front end</div>
        <Router>
          <div>
            <nav>
              <ul>
                <li><Link to="/home">Home</Link></li>
                <li><Link to="/bank">Bank</Link></li>
                <li><Link to="/client?clientId=123">Client</Link></li>
              </ul>
            </nav>
            <Switch>
              <Route path="/client">
                <Client />
              </Route>
              <Route path="/bank">
                <h1>BANK INFO</h1>
              </Route>
              <Route path="/home">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
    </div>
  );
}

export default App;
