import React from 'react';
import axios from 'axios';
import { Card } from 'base-components';

class Client extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      client: {},
      error: null,
    };
    this.fetchClients = this.fetchClients.bind(this);
  }

  fetchClients(id) {
    axios.get(`http://localhost:7890/client?clientId=${id}`).then(res => {
      this.setState({ client: res.data.client });
    });
  }

  componentDidMount() {
    this.fetchClients(123);
  }

  render() {
    return (
     <div>
       <h1>Client page</h1>
       <Card title={this.state.client.name} contents={`DOB: ${this.state.client.dob}`} />
     </div>
    );
  }
}

export default Client;
