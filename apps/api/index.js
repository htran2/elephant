const express = require('express');

const PORT = process.env.PORT || 7890;
const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/health', (req, res) => {
  res.send({
    message: 'API in great shape :)'
  });
})

app.get('/client', (req, res) => {
  if(req.query.clientId === '123') {
    res.send({
      client: {
        name: 'Ursula Magdalena Mustafi',
        dob: '1984-03-31',
      }
    });
  } else {
    res.send({
      client: {
        name: 'John Very Slow',
        dob: '2000-06-14',
      }
    });
  }
})

app.get('/bank-info', (req, res) => {
  res.send({
    name: 'Super Bank',
    primeRatePercent: 3.25,
    address: '18323 - Dream Avenue, Amsterdam, Holland',
  });
})


const advertise = () => {
  console.log(`API ready.`)
  console.log(`Try localhost:${PORT}/health`)
  console.log(`Try localhost:${PORT}/bank-info`)
  console.log(`Try localhost:${PORT}/client?clientId=123`)
}
app.listen(PORT, advertise);
