"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storyOne = exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _addonKnobs = require("@storybook/addon-knobs");

var _Card = _interopRequireDefault(require("./Card"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  title: 'Cards',
  decorators: [_addonKnobs.withKnobs]
};
exports["default"] = _default;

var storyOne = function storyOne() {
  var contents = (0, _addonKnobs.text)("Contents", "Stuffs inside");
  return /*#__PURE__*/_react["default"].createElement(_Card["default"], {
    title: (0, _addonKnobs.text)("Title", "The thing on top"),
    contents: contents
  });
};

exports.storyOne = storyOne;