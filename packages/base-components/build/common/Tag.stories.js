"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storyTwo = exports.storyOne = exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _addonKnobs = require("@storybook/addon-knobs");

var _Tag = _interopRequireDefault(require("./Tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  title: 'Tags',
  decorators: [_addonKnobs.withKnobs]
};
exports["default"] = _default;

var storyOne = function storyOne() {
  return /*#__PURE__*/_react["default"].createElement(_Tag["default"], {
    text: "ONE",
    colour: "salmon"
  });
};

exports.storyOne = storyOne;

var storyTwo = function storyTwo() {
  return /*#__PURE__*/_react["default"].createElement(_Tag["default"], {
    text: (0, _addonKnobs.text)("Caption", "change me"),
    colour: "gold"
  });
};

exports.storyTwo = storyTwo;