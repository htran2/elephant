import { configure, addDecorator, addParameters } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs';
import '!style-loader!css-loader!sass-loader!../src/storybook.scss';
import '!style-loader!css-loader!sass-loader!../src/index.scss';

addParameters({ options: { panelPosition: 'right' } });
addDecorator(withInfo);
addDecorator(withKnobs);

addDecorator((story) => {
  // add global styles
  document.body.classList.add('elephant-canvas');
  return story();
});

const req = require.context('./../src', true, /.stories\.js$/);

function loadStories() {
  req.keys().forEach(req);
}

configure(loadStories, module);
