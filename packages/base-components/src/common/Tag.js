import React, { Component } from 'react';

class Tag extends Component {
  render() {
   const s = { background: this.props.colour };
    return (
      <div className="tag" style={s}>
        <div>{this.props.text}</div>
      </div>
    );
  }
}

export default Tag;
