import React, { PureComponent } from 'react';
// import { Card as AntCard, Button } from 'antd';

class Card extends React.PureComponent {
  render() {
    return (
      <div className="card">
        <h1>{this.props.title}</h1>
        {this.props.contents}
      </div>
    );
  }
}

export default Card;
