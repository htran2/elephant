import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import Card from './Card';

export default { title: 'Cards', decorators: [withKnobs] };

export const storyOne = () => {
  const contents = text("Contents", "Stuffs inside");
  return <Card title={text("Title", "The thing on top")} contents={contents} />;
};
