import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import Tag from './Tag';

export default { title: 'Tags', decorators: [withKnobs] };

export const storyOne = () => <Tag text="ONE" colour="salmon"/>;
export const storyTwo = () => <Tag text={text("Caption", "change me")} colour="gold"/>;
