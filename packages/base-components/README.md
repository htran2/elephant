Base UI components, for any business.

First setup for local development:
`cd .......elephant/packages/base-components`
`npm run build`
`npm run build-storybook`
`npm run storybook`

Everyday development:
To add a (React) component `X`:
- add `X.js`, `X.scss`, `X.stories.js` under `src/folder_for_x`
- import and export `X` in `src/index.js`
- import `X.scss` in `src/index.scss`
- Visual testing: `npm run storybook`
- Build: `npm run build` to transpile all JSs and SCSSs into `build/index.js` and `build/index.css`

From outside this lerna package, to use `X` you need:
- to add dependency `base-components` in `package.json`
- `import 'base-components/build/index.css'` at top level (e.g. `index.js`)
- In your class, `import { X } from 'base-components'` then use it as `<X />`


Build storybook to be run as a static app:
`npm run build-storybook`
`npx http-server .storybook/build`  (create a faked server and show the storybook)
