import React, { Component } from 'react';

export default class BankHeader extends Component {
  render() {
    return <h1 className="bankHeader">Super Bank</h1>;

  /* Dear Amogh,
  Can you try, to have api and base-components as dependencies, then:
  - axios to localhost:7890/bank-info to get some data, save in this.state
  - import Card from base-components, display here, something like:
    <Card title=this.state.bankName>
      {this.state.bankAddress}
    </Card>
  */
  }
}
