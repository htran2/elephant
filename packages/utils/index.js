module.exports = {
  validators: {
    isEmpty: (value) => value === undefined || value === null || !value.trim(),
  },
  formatters: {
    formatDate: (date) => date ? date.toLocaleDateString('fr-ca') : 'N/A',
    formatString: (s) => s ? s.trim() : 'n/a',
  }
}
