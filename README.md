# elephant

POC of a monorepo.
https://gitlab.com/htran2/elephant

Main/entry package is `packages/main`

`npm install lerna`

Clone this repo, then run:

`cd elephant`
`lerna bootstrap`

Now you can work on each module independently, e.g.:
`cd packages/base-components` and follow its `README.md`, or
`cd apps/api` and follow its `README.md`,
etc.

To get the front end to work:
- make sure all `packages/build` exist, and each package has run its `npm run build`
- follow `apps/frontend/README.md`

start the front end by following `apps/frontend/README.md`
